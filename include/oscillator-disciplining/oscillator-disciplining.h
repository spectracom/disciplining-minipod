/*
 * liboscillator-disciplining: Disciplining Algorithm for Orolia's mRO50.
 * Copyright (C) 2021  Spectracom SAS

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * @mainpage liboscillator-disciplining
 *
 * C library for disciplining an oscillator using miniCod algorithm.
 *
 * @see oscillator-disciplining.h for API documentation.
 * @see algorithm_structs.h for algorithm's structures definitions.
 */
/**
 * @file oscillator-disciplining.h
 * @brief liboscillator-disciplining's API header file.
 *
 * liboscillator-disciplining is a small library responsible of abstracting
 * disciplining algorithms used for an oscillator for which we want to control
 * the frequency.
 *
 */

#ifndef INCLUDE_OSCILLATOR_DISCIPLINING_OSCILLATOR_DISCIPLINING_H_
#define INCLUDE_OSCILLATOR_DISCIPLINING_OSCILLATOR_DISCIPLINING_H_
#include <time.h>
#include <stdbool.h>
#include <inttypes.h>

/**
 * @def SETTLING_TIME
 * @brief Time elapsed between each loop of the od_process loop
 */
#define SETTLING_TIME 6

/**
 * @def OD_ERR_MSG_LEN
 * @brief Required size an error message buffer must have.
 */
#define OD_ERR_MSG_LEN 0x400

/**
 * @struct minipod_config
 * @brief Minipod configuration
 */
struct minipod_config {
		/** Used to filter phase */
	int ref_fluctuations_ns;
	/** Threshold above which as phase jump is requested */
	int phase_jump_threshold_ns;
	/** Phasemeter's resolution in ns */
	int phase_resolution_ns;
	/** Enable debug logs */
	int debug;
	/** Minimal reactivity */
	int reactivity_min;
	/** Maximal reactivity */
	int reactivity_max;
	/** Power used in reactivity computation */
	int reactivity_power;
	/** number of phase error measures for one control node
	 * when doing a calibration
	 */
	int nb_calibration;
	/**
	 * Set tolerance range to check if fine equilibrium is inside this range
	 * to validate calibration.
	 * After a calibration fine equilibrium point must be between
	 * ctrl_range_fine[0] + fine_stop_tolerance and
	 * ctrl_range_fine[1] - fine_stop_tolerance.
	 */
	int fine_stop_tolerance;
	/** Maxium difference allowed when changin coarse value */
	int max_allowed_coarse;
	/** Triggers calibration when starting the program */
	bool calibrate_first;
	/** Define wether to use factory settings or not */
	bool oscillator_factory_settings;
};

/**
 * Maximum number of points that can be stored in the memory of the card
 */
#define CALIBRATION_POINTS_MAX 10

/**
 * @struct disciplining parameters
 * @brief Disciplining parameters corresponding to mRO50 device disciplined
 *
 */
struct disciplining_parameters {
	/**
	 * Array containing the control node, in percentage
	 * value of the control range.
	 * Array contains ctrl_nodes_length valid values.
	 */
	float ctrl_load_nodes[CALIBRATION_POINTS_MAX];
	/**
	 * Array of drift coefficients for each control node.
	 * Array contains ctrl_nodes_length valid values.
	 */
	float ctrl_drift_coeffs[CALIBRATION_POINTS_MAX];
	/** Equilibrium Coarse value define during calibration */
	/**
	 * Array containing the control node, in percentage
	 * value of the control range.
	 * Array contains ctrl_nodes_length_factory valid values.
	 */
	float ctrl_load_nodes_factory[3];
	/**
	 * Array of drift coefficients for each control node.
	 * Array contains ctrl_nodes_length_factory valid values.
	 */
	float ctrl_drift_coeffs_factory[3];
	/** Equilibrium Coarse value for factory_settings */
	int32_t coarse_equilibrium_factory;
	int32_t coarse_equilibrium;
	/** Date at which calibration has been made */
	time_t calibration_date;
	/** Factory Settings that can be used with any mRO50 */
	/** Number of control nodes in ctrl_load_nodes_factory */
	uint8_t ctrl_nodes_length_factory;
	/** Number of control nodes in ctrl_load_nodes */
	uint8_t ctrl_nodes_length;
	/** Indicate wether calibration parameters are valid */
	bool calibration_valid;
};

/**
 * @struct od_input
 * @brief Structure containing all the input parameters for the disciplining
 * algorithm.
 */
struct od_input {
	/** Calibration requested by software of user */
	bool calibration_requested;
	/** Coarse adjustement setpoint */
	int32_t coarse_setpoint;
	/** Fine adjustement setpoint */
	uint32_t fine_setpoint;
	/** is mRO locked */
	bool lock;
	/** phase error measured between the oscillator and the GNSS */
	struct timespec phase_error;
	/** temperature, only used for logging. */
	double temperature;
	/** is GNSS available (and hence, is the phase error meaningful) */
	bool valid;
};

/**
 * @enum output_action
 * @brief Enumeration of the possible action that must be done to control
 * the oscillator
 */
enum output_action {
	NO_OP,
	PHASE_JUMP,
	ADJUST_FINE,
	ADJUST_COARSE,
	CALIBRATE,
	SAVE_DISCIPLINING_PARAMETERS,
	NUM_ACTIONS,
};

/**
 * @struct od_output
 * @brief Structure which will be fed with the algorithm's outputs after
 * returning from od_process.
 */

struct od_output {
	/** frequence adjustment value, ranges depend wether concerning
	 * fine or coarse alignement:
	 * coarse: [0, 4194303]
	 * fine: [1600, 3200]
	 */
	uint32_t setpoint;
	/** Indicate action that should be done */
	enum output_action action;
	/** value of the phase jump */
	int32_t value_phase_ctrl;
};

/**
 * @enum State
 * @brief Algorithm state value
 */
enum Disciplining_State {
	/** Initialization State */
	INIT,
	/** Phase adjustement State, nominal one */
	PHASE_ADJUSTMENT,
	/** Holdover state, when gnss data is not valid */
	HOLDOVER,
	/** Calibration state, when drift coefficients are computed */
	CALIBRATION,
};

/**
 * @struct od
 * @brief Opaque library context.
 */
struct od;

/**
 * @struct calibration_parameters
 * @brief structure used to pass parameters for mRO50's calibration
 */
struct calibration_parameters {
	/** control points used for the calibration */
	uint16_t *ctrl_points;
	/** Number of control points */
	int length;
	/** For each control points, nb_calibration measures 
	 * of the phase error must be done
	 */
	int nb_calibration;
};

/**
 * @struct calibration_results
 * @brief structure containing the results of a calibration done
 * from calibration_parameters structures.
 */
struct calibration_results {
	/** all results are stored in timespec double array of pointer
	 * calib_results can be represented as
	 * a double array of size length * nb_calibration
	*/
	struct timespec * measures;
	/** Number of control points.
	 * Should be equal to calibration_parameters.length
	 */
	int length;
	/** For each control points, number of phase error measures done.
	 * Should be equal to calibration_parameters.nb_calibration
	 */
	int nb_calibration;
};


/**
 * @brief Creates a liboscillator-disciplining context.
 * @param minipod_config Algorithm parameters
 * @param disciplining_config Disciplining parameters specific to mRO50 being disciplined
 * @param err_msg Char buffer of size OD_ERR_MSG_LEN which will contain
 * additional human readable information on failure.
 * @return Context newly created, which must be destroyed by calling
 * od_destroy().
 */
struct od *od_new_from_config(struct minipod_config *minipod_config, struct disciplining_parameters *disciplining_config, char err_msg[OD_ERR_MSG_LEN]);
/**
 * @brief Processes input data using the disciplining algorithm.
 * @param od Library context.
 * @param input Input parameters fed into the algorithm.
 * @param output Output data produced by the algorithm step ran.
 * @return 0 on success, errno-compatible negative value or error.
 */
int od_process(struct od *od, const struct od_input *input,
		struct od_output *output);



/**
 * @brief Output disciplining parameters used by minipod.
 * @param od Library context.
 * @param disciplining_parameters pointer to output disciplining parameters.
 * @return int, 0 on success.
 */
int od_get_disciplining_parameters(struct od *od, struct disciplining_parameters *disciplining_parameters);

/**
 * @brief Outputs parameters needed for the mRO50 calibration.
 * @param od Library context.
 * @return struct calibration_parameters.
 */
struct calibration_parameters * od_get_calibration_parameters(struct od *od);

/**
 * @brief Process calibration results to compute phase time and ctrl coeffs.
 * @param od Library context.
 * @param calib_params calibration parameters obtained from od_get_calibration_paramters.
 * @param calib_results calibration measures done by the daemon.
 * @return 0 on success, errno-compatible negative value or error.
 */

void od_calibrate(struct od *od, struct calibration_parameters *calib_params, struct calibration_results *calib_results);

/**
 * @brief Destroys a context previously created by a call to od_new().
 * @param od Context to destroy, put to NULL in output, can be NULL or point to
 * NULL.
 */
void od_destroy(struct od **od);

/**
 * @brief Returns current algorithm status
 * @param od Algorithm context.
 * @return enum Disciplining_State as an int
 */
int od_get_status(struct od *od);

#endif /* INCLUDE_OSCILLATOR_DISCIPLINING_OSCILLATOR_DISCIPLINING_H_ */
